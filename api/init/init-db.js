var UserGroupsRights = require('../models/settings/user-groups-rights');

exports.createRoutes = function (router) {
	message = "";
	routesPath = [];
	apiRoutesPathMethods = [];
	router.stack.forEach(function (apiRoute) {
		routesPath.push(apiRoute.route.path);
		apiRoutesPathMethods.push({ route: apiRoute.route.path, methods: apiRoute.route.methods })
	})
	UserGroupsRights.find({ route: { $in: routesPath } }, function (err, dbRoutes) {
		if (err) {
			console.log(err)
		}
		apiRoutesPathMethods.forEach(function (route) {
			updateMethod = [];
			routeMethods = route.methods;
			if (routeMethods.get) { updateMethod.push('GET') } else { index = updateMethod.indexOf('GET'); if (index != -1) { updateMethod.splice(index, 1); } }
			if (routeMethods.put) { updateMethod.push('PUT') } else { index = updateMethod.indexOf('PUT'); if (index != -1) { updateMethod.splice(index, 1); } }
			if (routeMethods.post) { updateMethod.push('POST') } else { index = updateMethod.indexOf('POST'); if (index != -1) { updateMethod.splice(index, 1); } }
			if (routeMethods.delete) { updateMethod.push('DELETE') } else { index = updateMethod.indexOf('DELETE'); if (index != -1) { updateMethod.splice(index, 1); } }

			thisDbRoute = dbRoutes.find(function (dbRoute) { return dbRoute.route === route.route })
			if (thisDbRoute) {
				newDbRoute = thisDbRoute;
				newDbRoute.methods = updateMethod;
				message += thisDbRoute + ' has been updated \n';
			} else {
				var newDbRoute = new UserGroupsRights();
				newDbRoute.route = route.route;
				newDbRoute.methods = updateMethod;
				message += thisDbRoute + ' has been Created \n';
			}
			// console.log(newDbRoute)
			newDbRoute.save(function (err) {
				if (err) {
					console.log(err)
				}
			})
		})
	})
	console.log(message);
}