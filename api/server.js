
var config = require('./config');

// Import middleware
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var morgan = require('morgan')

// Import controllers
var contactController = require('./controllers/contact');
var userController = require('./controllers/user');
var issueController = require('./controllers/issue');
var projectController = require('./controllers/project');

// Import settings Controllers
var userGroupController = require('./controllers/settings/user-groups');
var userGroupRightController = require('./controllers/settings/user-groups-rights');

// Import init
var initDbController = require('./init/init-db');

mongoose.connect(config.DB + config.DB_Name, function (err) {
	if (err) {
		return err;
	} else {
		console.log('API connected to ' + config.DB_Name);
	}
});

var app = express()
var authController = require('./controllers/auth');
var rightsController = require('./controllers/rights');

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({
	extended: true
}))
app.use(bodyParser.json());
app.use(function (req, res, next) {
	// Website you wish to allow to connect
	res.setHeader('Access-Control-Allow-Origin', '*');
	// Request methods you wish to allow
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	// Request headers you wish to allow
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Access-Control-Allow-Origin,x-access-token');
	// Set to true if you need the website to include cookies in the requests sent
	// to the API (e.g. in case you use sessions)
	res.setHeader('Access-Control-Allow-Credentials', true);
	// Pass to next layer of middleware
	next();
});

var port = process.env.port || 4200;

var router = express.Router();

// Contact Routes
router.route('/contacts')
	.post( contactController.postContact)
	.get(rightsController.postRights, contactController.getContacts);

router.route('/contact/:contact_id')
	.get(rightsController.postRights, contactController.getContact)
	.put(rightsController.postRights, contactController.updateContact)
	.delete(rightsController.postRights, contactController.deleteContact);

// User Routes
router.route('/users')
	.post(userController.postUser)
	.get(authController.checkAuth, userController.getUsers);

router.route('/user/:user_id')
	.get(userController.getUser)
	.put(authController.checkAuth, userController.updateUser)
	.delete(authController.checkAuth, userController.deleteUser);

router.route('/users/:user_email')
	.get(userController.getUserEmail)

// Issues Routes
router.route('/issues')
	.post(issueController.postIssue)
	.get(authController.checkAuth, issueController.getIssues);

router.route('/issue/:issue_id')
	.get(issueController.getIssue)
	.put(issueController.updateIssue)
	.delete(issueController.deleteIssue);

// Project Route
router.route('/projects')
	.post(authController.checkAuth, rightsController.postRights, projectController.postProject)
	.get(authController.checkAuth, rightsController.postRights, projectController.getProjects);

router.route('/project/:projectId')
	.get(authController.checkAuth, rightsController.postRights, projectController.getProject)
	.put(authController.checkAuth, rightsController.postRights, projectController.updateProject)
	.delete(authController.checkAuth, rightsController.postRights, projectController.deleteProject);

// Route auth
router.route('/authenticate')
	.post(authController.postAuth)
	.get(authController.readToken);

// Settings route
router.route('/settings/usergroups')
	.post(userGroupController.postUserGroup)
	.get(userGroupController.getUserGroups);
router.route('/settings/usergroup/:userGroup_id')
	.get(userGroupController.getUserGroup)
	.put(userGroupController.updateUserGroup)
	.delete(userGroupController.deleteUserGroup);
router.route('/settings/usergrouprights')
	.post(userGroupRightController.postUserGroupsRights)
	.get(userGroupRightController.getUserGroupsRights);
router.route('/settings/usergroupright/:route_id')
	.get(userGroupRightController.getUserGroupsRight)
	.put(userGroupRightController.updateUserGroupsRight)
	.delete(userGroupRightController.deleteUserGroupsRight);



router.get('/', function (req, res) {
	res.json({ message: 'Welcome on Liruta API' });
})

app.use('/api', router);

app.listen(port);
console.log('LirutaAPI is up and running on port ' + port);

initDbController.createRoutes(router);

