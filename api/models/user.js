var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var UserSchema = new mongoose.Schema({
	email: {
		type: String,
		required: true,
		unique: true
	},
	password: {
		type: String,
		required: true
	},
	username: String,
	canLogIn: Boolean,
	isAdmin: Boolean,
	groups: Array,
	created_at: Date,
	updated_at: Date
})


UserSchema.pre('save', function (callback) {
	var user = this;
	var currentDate = new Date();
	user.updated_at = currentDate;
	if (!user.created_at) {
		user.created_at = currentDate;
	}
	if (!user.groups) { user.groups = '' };
	if (!user.isAdmin) { user.isAdmin = false };
	if (!user.canLogIn) { user.canLogIn = false };
	
	if (!user.isModified('password')) { return callback() } else {
		console.log('password is modified');
		bcrypt.genSalt(5, function (err, salt) {
			if (err) {
				return callback(err);
			}
			bcrypt.hash(user.password, salt, null, function (err, hash) {
				if (err) {
					return callback(err);
				}
				user.password = hash;

				callback();
			})
		})
	}

})

UserSchema.methods.verifyPassword = function (password, cb) {
	console.log(password, this.password);
	bcrypt.compare(password, this.password, function (err, isMatch) {
		if (err) return cb(err);
		cb(null, isMatch);
	});
};

module.exports = mongoose.model('User', UserSchema); 