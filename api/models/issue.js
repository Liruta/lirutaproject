var mongoose = require('mongoose');

var IssuesSchema = new mongoose.Schema({
	relatedTo: String, // Will be a contact first and then why not something else
	relatedToId: String, // Will be replaced with the ID of the thing related to
	name: String,
	description: String,
	createdBy: String, // to be replace with user
	status: String,
	archived: Boolean,
	created_at: Date,
	updated_at: Date
})


IssuesSchema.pre('save', function (callback) {
	var contact = this;
	var currentDate = new Date();
	contact.updated_at = currentDate;
	if (!contact.created_at) { contact.created_at = currentDate; }
	if (!contact.archived) { contact.archived = false; }
	if (!contact.createdBy) { contact.createdBy = 'Nobody: Error System (check the API)'; }
	if (!contact.status) { contact.status = 'Pending'; }
	if (!contact.relatedTo) { contact.relatedTo = 'Contact'; }
	callback();
})


module.exports = mongoose.model('Issues', IssuesSchema);