var mongoose = require('mongoose');

var UserGroupSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	description: String,
	bcColor: String,
	txtColor: String,
})


UserGroupSchema.pre('save', function (callback) {
	userGroup = this;
	if(!userGroup.bcColor) { userGroup.bcColor = 'white' }
	if(!userGroup.txtColor) { userGroup.txtColor = 'black' }
	callback();
})


module.exports = mongoose.model('UserGroup', UserGroupSchema);