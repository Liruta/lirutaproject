var mongoose = require('mongoose');
var User = require('./../user');

var UserGroupsRightsSchema = new mongoose.Schema({
	route: {
		type: String,
		require: true
	},
	methods: Array,
	getGroups: Array,
	putGroups: Array,
	postGroups: Array,
	deleteGroups: Array,
	updatedAt: Date,
	createdAt: Date,
	updatedBy: String
})


UserGroupsRightsSchema.pre('save', function (callback) {
	right = this;
	var currentDate = new Date();
	right.updatedAt = currentDate;
	if(!right.createdAt) { right.createdAt = currentDate }
	if (!right.methods) { right.methods = '' }
	if (!right.getGroups) { right.getGroups = '' }
	if (!right.putGroups) { right.putGroups = '' }
	if (!right.postGroups) { right.postGroups = '' }
	if (!right.deleteGroups) { right.deleteGroups = '' }
	callback();
})


module.exports = mongoose.model('UserGroupsRights', UserGroupsRightsSchema);