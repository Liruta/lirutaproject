var mongoose = require('mongoose');
var User = require('../models/user');

var ProjectSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	description: String,
	picture: String,
	createdBy: String,
	userInvolved: [{
		type: String,
		ref: 'User'
	}],
	created_at: Date,
})

ProjectSchema.pre('save', function (callback) {
	var project = this;
	var currentDate = new Date();
	if (!project.created_at) { project.created_at = currentDate; }
	if (!project.createdBy) { project.createdBy = 'Created by the system'; }
	callback();
})


module.exports = mongoose.model('Project', ProjectSchema);