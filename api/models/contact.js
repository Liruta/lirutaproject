var mongoose = require('mongoose');

var ContactSchema = new mongoose.Schema({
	email: {
		type: String,
		required: true
	},
	name: String,
	lastname: String,
	picture: String,
	phone: String,
	created_at: Date,
	updated_at: Date
})


ContactSchema.pre('save', function (callback) {
	var contact = this;
	var currentDate = new Date();
	contact.updated_at = currentDate;
	if (!contact.created_at) {
		contact.created_at = currentDate;
	}
	callback();
})


module.exports = mongoose.model('Contact', ContactSchema);