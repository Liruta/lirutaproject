var Project = require('../models/project');
var User = require('../models/user');


exports.postProject = function (req, res) {
	var project = new Project();

	if (!req.body.name) {
		res.json({ message: 'Name field cannot be empty' }).status(400);
	}
	project.name = req.body.name;
	project.description = req.body.description;
	project.picture = req.body.picture;
	project.createdBy = req.body.createdBy;
	project.userInvolved = req.body.userInvolved;

	project.save(function (err) {
		if (err) {
			res.send(err).status(501);
		}
		res.json({ success: true,  message: 'New project has been added!', data: project }).status(201);
	})
}

exports.getProjects = function (req, res) {
	Project.find(function (err, projects) {
		if (err) {
			res.send(err).status(404);
		}
		res.json(projects).status(200);
	})
}

exports.getProject = function (req, res) {
	Project.findById(req.params.projectId, function (err, project) {
		if (err) {
			res.send(err).status(404);
		}
		res.json(project).status(200);
	}).populate('userInvolved', 'email username')
}

exports.updateProject = function (req, res) {
	Project.findById(req.params.projectId, function (err, project) {
		if (err) {
			res.send(err).status(404);
		}
		if (req.body.name) { project.name = req.body.name; }
		if (req.body.description) { project.description = req.body.description; }
		if (req.body.picture) { project.picture = req.body.picture; }
		if (req.body.userInvolved) { project.userInvolved = req.body.userInvolved; }

		project.save(function (err) {
			if (err) {
				res.send(err).status(501);
			}
			res.json({ success: true,  message: 'Project ' + project._id + ' has been updated', data: project }).status(200);
		})
	})
}

exports.deleteProject = function (req, res) {
	Project.findByIdAndRemove(req.params.projectId, function (err) {
		if (err) {
			res.send(err).status(501);
		}
		res.json({ success: true, message: 'Contact has been removed from the database' }).status(200)
	})
}