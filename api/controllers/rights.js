var User = require('../models/user');
var UserGroup = require('../models/settings/user-groups');
var RouteRights = require('../models/settings/user-groups-rights');
var jwt = require('jsonwebtoken');
var config = require('./../config');

exports.postRights = function (req, res, next) {
    var token = req.headers['x-access-token'];
    req.isAuth = false;
    if (token) {
        jwt.verify(token, config.secret, function (err, decoded) {
            if (err) {
                return res.json({ success: false, message: 'Failed to decode the token' }).status(500)
            } else {
                var url = req.originalUrl;
                var route = url.slice(4, url.length); // remove the /api
                var index = route.lastIndexOf('/');
                // console.log(index)
                if (index > 0) {
                    route = route.slice(0, index + 1);
                }
                // console.log(route);
                var method = req.method;

                var regexp = new RegExp("^" + route);
                RouteRights.findOne({ 'route': regexp }, function (err, routeRight) {
                    if (err) {
                        return res.json({ success: false, message: err }).status(404);
                    }
                    // console.log(routeRight)
                    if (method == 'GET') { groupsToCheck = routeRight.getGroups; }
                    if (method == 'POST') { groupsToCheck = routeRight.postGroups; }
                    if (method == 'PUT') { groupsToCheck = routeRight.putGroups; }
                    if (method == 'DELETE') { groupsToCheck = routeRight.deleteGroups; }

                    decoded._doc.groups.forEach(function (groupId) {
                        groupsToCheck.forEach(function (groupRightId) {
                            // console.log(groupId, groupRightId);
                            if (groupId === groupRightId) {
                                req.isAuth = true;
                                // return next();
                            }
                        })
                    })
                    // console.log('IsAuth: ', req.isAuth)

                    if (req.isAuth) {
                        return next();
                    } else if (req.isAuth == false) {
                        return res.status(403).json({ success: false, message: 'You don\'t have the right to access to this route' })
                    }
                })
            }
        })
    } else {
        return res.status(403).json({ success: false, message: 'No token provided.' })
    }


}