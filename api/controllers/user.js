var User = require('../models/user');

// Change this value to able users to log in by default or not
var canLogIn = true;

exports.postUser = function (req, res) {
	var user = new User({
		email: req.body.email,
		password: req.body.password
	});

	if (!req.body.email) {
		res.json({ success: false, message: 'email field cannot be empty' }).status(400);
	}
	if (!req.body.password) {
		res.json({ success: false, message: 'password field cannot be empty' }).status(400);
	}

	user.canLogIn = canLogIn;

	user.save(function (err) {
		if (err) {
			res.send(err).status(501);
		}
		res.json({ message: 'New User has been added!', data: user, status: 'Success' }).status(201);
		// console.log('New User has been Created');
		// console.log('New User has been Created');
	})
}

exports.getUsers = function (req, res) {
	if (req.decoded._doc.isAdmin) { // Check if user is admin
		User.find(function (err, users) {
			if (err) {
				res.send(err).status(404);
			}
			res.json(users).status(200);
			// console.log('users has been requested');
		})
	} else {
		res.json({ success: false, message: 'You are not administrator' }).status(401)
	}
}

exports.getUser = function (req, res) {
	User.findById(req.params.user_id, function (err, user) {
		if (err) {
			res.send(err).status(404);
		}
		res.json(user).status(200);
		// console.log('User ' + req.params.user_id + ' has been requested');
	})
}


exports.getUserEmail = function (req, res) {
	User.findOne({ 'email': req.params.user_email }, function (err, user) {
		if (err) {
			res.send(err).status(404);
		}
		res.json(user).status(200);
		// console.log('User ' + req.params.user_email + ' has been requested');
	})
}


exports.updateUser = function (req, res) {
	User.findById(req.params.user_id, function (err, user) {
		if (err) {
			res.send(err).status(404);
		}
		if (req.body.email) { user.email = req.body.email; }
		if (req.body.password) { user.password = req.body.password; }
		if (req.body.canLogIn === false || req.body.canLogIn) { user.canLogIn = req.body.canLogIn; }
		if (req.body.groups) { user.groups = req.body.groups; }
		if (req.body.isAdmin === false || req.body.isAdmin) { user.isAdmin = req.body.isAdmin; }

		user.save(function (err) {
			if (err) {
				res.send(err).status(501);
			}
			res.json({ message: 'User ' + user._id + ' has been updated', data: user }).status(200);
			// console.log('User ' + req.params.user_id + ' has been updated');
		})
	})
}

exports.deleteUser = function (req, res) {
	User.findByIdAndRemove(req.params.user_id, function (err) {
		if (err) {
			res.send(err).status(501);
		}
		res.json({ message: 'User has been removed from the database' }).status(200)
		// console.log('User ' + req.params.user_id + ' has been deleted');
	})
}