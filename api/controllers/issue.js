var Issue = require('../models/issue');


exports.postIssue = function (req, res) {
	var issue = new Issue();
	
	issue.name = req.body.name;
	issue.description = req.body.description;

	issue.save(function (err) {
		if (err) {
			res.send(err).status(501);
		}
		res.json({ message: 'New issue has been added!', data: issue }).status(201);
		// console.log('New issue has been Created');
	})
}

exports.getIssues = function (req, res) {
	Issue.find(function (err, issues) {
		if (err) {
			res.send(err).status(404);
		}
		res.json(issues).status(200);
		// console.log('Issues has been requested');
	})
}

exports.getIssue = function (req, res) {
	Issue.findById(req.params.issue_id, function (err, contact) {
		if (err) {
			res.send(err).status(404);
		}
		res.json(contact).status(200);
		// console.log('Issue ' + req.params.issue_id + ' has been requested');
	})
}

exports.updateIssue = function (req, res) {
	Issue.findById(req.params.issue_id, function (err, issue) {
		if (err) {
			res.send(err).status(404);
		}
		if (req.body.description) { issue.description = req.body.description; }

		issue.save(function (err) {
			if (err) {
				res.send(err).status(501);
			}
			res.json({ message: 'Issue ' + issue._id + ' has been updated', data: issue }).status(200);
			// console.log('Iontact ' + req.params.issue_id + ' has been updated');

		})
	})
}

exports.deleteIssue = function (req, res) {
	Issue.findByIdAndRemove(req.params.issue_id, function (err) {
		if (err) {
			res.send(err).status(501);
		}
		res.json({ message: 'Issue has been removed from the database' }).status(200)
		// console.log('Issue ' + req.params.issue_id + ' has been deleted');
	})
}