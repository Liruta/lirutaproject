var Contact = require('../models/contact');


exports.postContact = function (req, res) {
	var contact = new Contact();

	if (!req.body.email) {
		res.json({ message: 'email field cannot be empty' }).status(400);
	}
	contact.email = req.body.email;
	contact.name = req.body.name;
	contact.lastname = req.body.lastname;
	contact.phone = req.body.phone;

	contact.save(function (err) {
		if (err) {
			res.send(err).status(501);
		}
		res.json({ message: 'New contact has been added!', data: contact }).status(201);
		// console.log('New contacts has been Created');
	})
}

exports.getContacts = function (req, res) {
	// console.log(res._headers);
	Contact.find(function (err, contacts) {
		if (err) {
			res.send(err).status(404);
		}
		res.json(contacts).status(200);
		// console.log('Contacts has been requested');
	})
}

exports.getContact = function (req, res) {
	Contact.findById(req.params.contact_id, function (err, contact) {
		if (err) {
			res.send(err).status(404);
		}
		res.json(contact).status(200);
		// console.log('Contact ' + req.params.contact_id + ' has been requested');
	})
}

exports.updateContact = function (req, res) {
	Contact.findById(req.params.contact_id, function (err, contact) {
		if (err) {
			res.send(err).status(404);
		}
		if (req.body.email) { contact.email = req.body.email; }
		if (req.body.name) { contact.name = req.body.name; }
		if (req.body.lastname) { contact.lastname = req.body.lastname; }
		if (req.body.phone) { contact.phone = req.body.phone; }

		contact.save(function (err) {
			if (err) {
				res.send(err).status(501);
			}
			res.json({ message: 'Contact ' + contact._id + ' has been updated', data: contact }).status(200);
			// console.log('Contact ' + req.params.contact_id + ' has been updated');

		})
	})
}

exports.deleteContact = function (req, res) {
	Contact.findByIdAndRemove(req.params.contact_id, function (err) {
		if (err) {
			res.send(err).status(501);
		}
		res.json({ message: 'Contact has been removed from the database' }).status(200)
		// console.log('Contact ' + req.params.contact_id + ' has been deleted');
	})
}