var UserGroups = require('../../models/settings/user-groups');

exports.getUserGroups = function (req, res) {
	UserGroups.find({}, function (err, userGroups) {
		if (err) {
			res.json({ success: false, message: err }).status(404);
		}
		res.json(userGroups).status(200)
	})
}

exports.postUserGroup = function (req, res) {
	var userGroup = new UserGroups();

	userGroup.name = req.body.name;
	userGroup.description = req.body.description;
	userGroup.bcColor = req.body.bcColor;
	userGroup.txtColor = req.body.txtColor;

	userGroup.save(function (err) {
		if (err) {
			res.json({ success: false, message: err }).status(500)
		}
		res.json({ success: true, message: 'New user group has been created', data: userGroup }).status(201);
	})
}

exports.getUserGroup = function (req, res) {
	UserGroups.findById(req.params.userGroup_id, function (err, userGroup) {
		if (err) {
			res.json({ success: false, message: err }).status(500)
		}
		res.json(userGroup).status(200);
	})
}

exports.updateUserGroup = function (req, res) {
	UserGroups.findById(req.params.userGroup_id, function (err, userGroup) {
		if (req.body.name) { userGroup.name = req.body.name }
		if (req.body.description) { userGroup.description = req.body.description }
		if (req.body.bcColor) { userGroup.bcColor = req.body.bcColor }
		if (req.body.txtColor) { userGroup.txtColor = req.body.txtColor }

		userGroup.save(function (err) {
			if (err) {
				res.json({ success: false, message: err }).status(500)
			}
			res.json({ success: true, message: 'User group ' + userGroup.name + ' has been updated', data: userGroup }).status(200);
		})
	})
}

exports.deleteUserGroup = function (req, res) {
	UserGroups.findByIdAndRemove(req.params.userGroup_id, function (err) {
		if (err) {
			res.json({ success: false, message: err }).status(501);
		}
		res.json({ success: true, message: 'User group ' + req.params.userGroup_id + ' has been removed' }).status(200);
	})
}