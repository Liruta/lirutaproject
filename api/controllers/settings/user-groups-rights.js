var UserGroupsRights = require('../../models/settings/user-groups-rights');

exports.getUserGroupsRights = function (req, res) {
    UserGroupsRights.find({}, function (err, UserGroupsRights) {
        if (err) {
            res.json({ success: false, message: err }).status(404);
        }
        res.json(UserGroupsRights).status(200)
    }).sort('createdAt')
}

exports.postUserGroupsRights = function (req, res) {
    var userGroupRight = new UserGroupsRights();

    userGroupRight.route = req.body.route;
    userGroupRight.methods = req.body.methods;

    userGroupRight.save(function (err) {
        if (err) {
            res.json({ success: false, message: err }).status(500)
        }
        res.json({ success: true, message: 'New route has been created', data: userGroupRight }).status(201);
    })
}

exports.getUserGroupsRight = function (req, res) {
    UserGroupsRights.findById(req.params.route_id, function (err, route) {
        if (err) {
            res.json({ success: false, message: err }).status(404)
        }
        res.json(route).status(200);
    })
}


exports.updateUserGroupsRight = function (req, res) {
	UserGroupsRights.findById(req.params.route_id, function (err, route) {
		if (req.body.route) { route.route = req.body.route }
		if (req.body.methods) { route.methods = req.body.methods }
		if (req.body.getGroups) { route.getGroups = req.body.getGroups }
		if (req.body.postGroups) { route.postGroups = req.body.postGroups }
		if (req.body.putGroups) { route.putGroups = req.body.putGroups }
		if (req.body.deleteGroups) { route.deleteGroups = req.body.deleteGroups }
		if (req.body.updatedBy) { route.updatedBy = req.body.updatedBy }


		route.save(function (err) {
			if (err) {
				res.json({ success: false, message: err }).status(500)
			}
			res.json({ success: true, message: 'Route ' + route.route + ' has been updated', data: route }).status(200);
		})
	})
}

exports.deleteUserGroupsRight = function (req, res) {
	UserGroupsRights.findByIdAndRemove(req.params.route_id, function (err) {
		if (err) {
			res.json({ success: false, message: err }).status(501);
		}
		res.json({ success: true, message: 'Route ' + req.params.route_id + ' has been removed' }).status(200);
	})
}