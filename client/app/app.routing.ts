import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent }   from './components/home/home.component';
import { ContactsComponent }   from './components/contacts/contacts.component';
import { ContactDetailsComponent } from './components/contact-details/contact-details.component';
import { LoginComponent }   from './components/login/login.component';
import { UsersComponent } from './components/users/users.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { SignupComponent } from './components/signup/signup.component';
import { IssuesComponent } from './components/issues/issues.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { ProjectDetailsComponent } from './components/project-details/project-details.component';

// Settings components
import { SettingsComponent } from './components/settings/settings.component';
import { UserGroupsSettingComponent } from './components/settings/user-groups/user-groups.component';
import { UserGroupsDetailsSettingComponent } from './components/settings/user-groups-details/user-groups-details.component';
import { UserGroupRightsComponent } from './components/settings/user-groups-rights/user-group-rights.component';




const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home',  component: HomeComponent },
  { path: 'contacts', component: ContactsComponent },
  { path: 'contact/:contact_id', component: ContactDetailsComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'users', component: UsersComponent },
  { path: 'user/:user_id', component: UserDetailsComponent },
  { path: 'issues', component: IssuesComponent },
  { path: 'projects', component: ProjectsComponent },
  { path: 'project/:project_id', component: ProjectDetailsComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'settings/usergroups', component: UserGroupsSettingComponent },
  { path: 'settings/usergroups/:usergroup_id', component: UserGroupsDetailsSettingComponent },
  { path: 'settings/usergroupsrights', component: UserGroupRightsComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}
