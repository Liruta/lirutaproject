import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// import of eternal helper
import { ColorPickerModule } from 'angular2-color-picker';
import { DropdownModule } from 'ng2-bootstrap/ng2-bootstrap';

import { AppRoutingModule } from './app.routing';


import { ApiConfig } from './config/api.config';
import { UsersService } from './services/user.service';
import { SettingsService } from './services/settings.service';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ContactsComponent } from './components/contacts/contacts.component';
import { ContactDetailsComponent } from './components/contact-details/contact-details.component';
import { LoginComponent } from './components/login/login.component';
import { UsersComponent } from './components/users/users.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { SignupComponent } from './components/signup/signup.component';
import { IssuesComponent } from './components/issues/issues.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { ProjectDetailsComponent } from './components/project-details/project-details.component';
// Settings Components
import { SettingsComponent } from './components/settings/settings.component';
import { UserGroupsSettingComponent } from './components/settings/user-groups/user-groups.component';
import { UserGroupsDetailsSettingComponent } from './components/settings/user-groups-details/user-groups-details.component';
import { UserGroupRightsComponent } from './components/settings/user-groups-rights/user-group-rights.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule,
    ColorPickerModule,
    DropdownModule
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    ContactsComponent,
    ContactDetailsComponent,
    LoginComponent,
    UsersComponent,
    UserDetailsComponent,
    SignupComponent,
    IssuesComponent,
    ProjectsComponent,
    ProjectDetailsComponent,
    SettingsComponent,
    UserGroupsSettingComponent,
    UserGroupsDetailsSettingComponent,
    UserGroupRightsComponent,
  ],
  bootstrap: [AppComponent],
  providers: [
    { provide: 'api', useClass: ApiConfig },
    UsersService,
    SettingsService

  ]
})
export class AppModule { }
