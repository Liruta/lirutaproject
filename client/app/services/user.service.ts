import { Injectable, Inject } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs/Subject';


@Injectable()
export class UsersService {
	headers: any;
	token = sessionStorage.getItem('token');

	constructor(
		@Inject('api') private api: any,
		private http: Http
	) {
		this.headers = new Headers();
		this.headers.append('Content-Type', 'application/json');
		this.headers.append('Access-Control-Allow-Origin', '*');
		if (this.token) {
			this.headers.append('x-access-token', this.token);
		}
	}

	// Login event
	private loginEventSource = new Subject<string>();

	loginEventObs = this.loginEventSource.asObservable();
	createLoginEvent(loginString: string) {
		this.loginEventSource.next(loginString);
	}
	// End login event
	getUserByEmail(email: string) {
		console.log(this.api.usersUrl + '/' + email);
		return this.http.get(this.api.usersUrl + '/' + email, { headers: this.headers })
			.map(user => user.json());
	}

	getUser(id: string) {
		return this.http.get(this.api.userUrl + id, { headers: this.headers })
			.map(user => user.json());
	}

	updateUser(user: any) {
		console.log(user);
		return this.http.put(this.api.userUrl + user._id, user, { headers: this.headers })
			.map(response => response.json());
	}


	postUsers(newUser: any) {
		return this.http.post(this.api.usersUrl, newUser, { headers: this.headers })
			.map(data => data.json())
	}

	getUsers() {
		return this.http.get(this.api.usersUrl, { headers: this.headers })
			.map(users => users.json());
	}

	getUserToken(email: string, password: string) {
		let credential = {
			email: email,
			password: password
		}
		return this.http.post(this.api.authUrl, JSON.stringify(credential), { headers: this.headers })
			.map(token => token.json());
	}

	readUserToken(token: string) {
		this.headers = new Headers();
		this.headers.append('Content-Type', 'application/json');
		this.headers.append('Access-Control-Allow-Origin', '*');
		this.headers.append('x-access-token', token);
		return this.http.get(this.api.authUrl, { headers: this.headers })
			.map(tokenDecoded => tokenDecoded.json());
	}
}