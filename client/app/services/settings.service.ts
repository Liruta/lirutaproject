import { Injectable, Inject } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class SettingsService {
	headers: any;
	token = sessionStorage.getItem('token');

	constructor(
		@Inject('api') private api: any,
		private http: Http
	) {
		this.headers = new Headers();
		this.headers.append('Content-Type', 'application/json');
		this.headers.append('Access-Control-Allow-Origin', '*');
		if (this.token) {
			this.headers.append('x-access-token', this.token);
		}
	}

	// UserGroups
	getUserGroups() {
		return this.http.get(this.api.userGroupsUrl, { headers: this.headers })
			.map(userGroups => userGroups.json());
	}
	postUserGroups(userGroup: any) {
		return this.http.post(this.api.userGroupsUrl, userGroup, { headers: this.headers })
			.map(response => response.json());
	}
	getUserGroup(id: string) {
		return this.http.get(this.api.userGroupUrl + id, { headers: this.headers })
			.map(userGroup => userGroup.json());
	}
	updateUserGroup(userGroup: any) {
		return this.http.put(this.api.userGroupUrl + userGroup._id, userGroup, { headers: this.headers })
			.map(response => response.json());
	}
	deleteUserGroup(id: string) {
		return this.http.delete(this.api.userGroupUrl + id, { headers: this.headers })
			.map(response => response.json());
	}

	getUserGroupRights() {
		return this.http.get(this.api.userGroupsRightsUrl, { headers: this.headers })
			.map(userGroupsRights => userGroupsRights.json());
	}
	postUserGroupRight(route: any) {
		return this.http.post(this.api.userGroupsRightsUrl, route, { headers: this.headers })
			.map(response => response.json());
	}
	getUserGroupRight(id: string) {
		return this.http.get(this.api.userGroupsRightUrl + id, { headers: this.headers })
			.map(route => route.json());
	}
	updateUserGroupRight(route: any) {
		return this.http.put(this.api.userGroupsRightUrl + route._id, route, { headers: this.headers })
			.map(response => response.json());
	}
	deleteUserGroupRight(id: string) {
		return this.http.delete(this.api.userGroupsRightUrl + id, { headers: this.headers })
			.map(response => response.json());
	}

}