import { Injectable, Inject } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class IssuesService {

    headers: any;
    token = sessionStorage.getItem('token');

    constructor(
        @Inject('api') private api: any,
        private http: Http
    ) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Access-Control-Allow-Origin', '*');
        if (this.token) {
            this.headers.append('x-access-token', this.token);
        }
    }

    getIssues() {
        return this.http.get(this.api.issuesUrl, { headers: this.headers })
            .map(issues => issues.json());
    }

    postIssue(issue: any) {
        return this.http.post(this.api.issuesUrl, JSON.stringify(issue), { headers: this.headers })
            .map(response => response.json());
    }

    getIssue(id: string) {
        return this.http.get(this.api.issueUrl + id, { headers: this.headers })
            .map(issue => issue.json())
    }

    updateIssue(issue: any) { //ID of this issue must be provided in the issue object
        return this.http.put(this.api.issueUrl + issue._id, { headers: this.headers })
            .map(response => response.json());
    }

    deleteIssue(id: string) {
        return this.http.delete(this.api.issueUrl + id, { headers: this.headers })
            .map(response => response.json());
    }
}