import { Injectable, Inject } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class ContactsService {
	headers: any;
	token = sessionStorage.getItem('token');

	constructor(
		@Inject('api') private api: any,
		private http: Http
	) {
		this.headers = new Headers();
		this.headers.append('Content-Type', 'application/json');
		this.headers.append('Access-Control-Allow-Origin', '*');
		if (this.token) {
			this.headers.append('x-access-token', this.token);
		}
	}

	private apiUrl = this.api.url;
	private contactsUrl = this.api.contactsUrl;
	private contactUrl = this.api.contactUrl;

	getContacts() {
		return this.http.get(this.contactsUrl, { headers: this.headers })
			.map(contacts => contacts.json());
	}

	getContact(id: string) {
		console.log(this.contactUrl + id)
		return this.http.get(this.contactUrl + id, { headers: this.headers })
			.map(contact => contact.json());
	}

	updateContact(contact: any) {
		return this.http.put(this.contactUrl + contact._id, JSON.stringify(contact), { headers: this.headers })
			.map(response => response.json());
	}

	postContact(contact: any) {
		return this.http.post(this.contactsUrl, contact, { headers: this.headers })
			.map(response => response.json());
	}

	deleteContact(contact_id: any) {
		return this.http.delete(this.contactUrl + contact_id, { headers: this.headers })
			.map(response => response.json());
	}
}