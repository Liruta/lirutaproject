import { Injectable, Inject } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class ProjectService {
    headers: any;
    token = sessionStorage.getItem('token');

    constructor(
        @Inject('api') private api: any,
        private http: Http
    ) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Access-Control-Allow-Origin', '*');
        if (this.token) {
            this.headers.append('x-access-token', this.token);
        }
    }

    getProjects() {
        return this.http.get(this.api.projectsUrl, { headers: this.headers })
            .map(projects => projects.json());
    }

    getProject(id: string) {
        return this.http.get(this.api.projectUrl + id, { headers: this.headers })
            .map(project => project.json())
    }

    postProject(project: any) {
        return this.http.post(this.api.projectsUrl, project, { headers: this.headers })
            .map(response => response.json());
    }

    updateProject(project: any) {
        return this.http.put(this.api.projectUrl + project._id, project, { headers: this.headers })
            .map(response => response.json());
    }

    deleteProject(id: string) {
        return this.http.delete(this.api.projectUrl + id, { headers: this.headers })
            .map(response => response.json());
    }

}