import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';
import { ColorPickerService } from 'angular2-color-picker';

import { SettingsService } from '../../../services/settings.service';

@Component({
	moduleId: module.id,
	selector: 'app-usergroups-details',
	templateUrl: 'user-groups-details.component.html',
	providers: [SettingsService]
})
export class UserGroupsDetailsSettingComponent implements OnInit {
	userGroup: any;

	constructor(
		private settingService: SettingsService,
		private route: ActivatedRoute,
		private router: Router,
		private location: Location,
		private cpService: ColorPickerService
	) { }

	ngOnInit() {
		this.getUserGroup();
	}

	getUserGroup() {
		this.route.params.forEach((params: Params) => {
			let id = params['usergroup_id'];
			if (id == 'new') {
				this.userGroup = {};
				return false;
			}
			this.settingService.getUserGroup(id)
				.subscribe(userGroup => {
					this.userGroup = userGroup;
				});
		});
	}

	saveUserGroup() {
		if (!this.userGroup._id) {
			// post user group
			this.settingService.postUserGroups(this.userGroup)
				.subscribe(response => {
					console.log(response);
				})
		} else {
			// update user Group
			this.settingService.updateUserGroup(this.userGroup)
				.subscribe(response => {
					console.log(response);
				})
		}
	}

	goBack() {
		this.location.back();
	}

	deleteUserGroup(){
		this.settingService.deleteUserGroup(this.userGroup._id)
			.subscribe(response => {
				console.log(response);
				this.goBack()
			})
	}
}