import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../services/settings.service';

@Component({
	moduleId: module.id,
	selector: 'app-user-groups-setting',
	templateUrl: 'user-groups.component.html',
	providers: [SettingsService]
})
export class UserGroupsSettingComponent implements OnInit {
	groups: any[];
	constructor(
		private settingService: SettingsService
	) { }

	ngOnInit() {
		this.getGroups();
	}

	getGroups() {
		this.settingService.getUserGroups()
			.subscribe(userGroups => {
				this.groups = userGroups;
				console.log(userGroups)
			})
	}
}