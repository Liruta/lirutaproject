import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../services/settings.service';


@Component({
	moduleId: module.id,
	selector: 'app-user-groups-rights',
	templateUrl: 'user-group-rights.component.html',
	providers: [SettingsService]
})
export class UserGroupRightsComponent implements OnInit {
	userGroups: any[];
	addRoute: boolean;
	newRoute: any = {};
	routes: any[];
	mock: any;

	constructor(
		private settingService: SettingsService
	) { }

	ngOnInit() {
		this.getGroups();
		this.getRoutes();
	}

	getGroups() {
		this.settingService.getUserGroups()
			.subscribe(userGroups => {
				this.userGroups = userGroups;
				console.log(userGroups)
			})
	}

	toggleAddRoute() {
		this.addRoute = !this.addRoute;
		if (!this.addRoute) {
			this.newRoute = {};
		}
	}

	getRoutes() {
		this.settingService.getUserGroupRights()
			.subscribe(userGroupRoutes => {
				this.routes = userGroupRoutes;
			})
	}

	addNewRoute() {
		if (this.newRoute.route != '') {
			if (this.newRoute._id) {
				this.settingService.updateUserGroupRight(this.newRoute)
					.subscribe(response => {
						console.log(response);
						this.getRoutes();
					})
			} else {
				this.settingService.postUserGroupRight(this.newRoute)
					.subscribe(response => {
						console.log(response);
						this.getRoutes();
					})
			}
			this.newRoute = {};
			this.addRoute = false;
		}
	}

	deleteRoute(route_id: string) {
		this.settingService.deleteUserGroupRight(route_id)
			.subscribe(response => {
				console.log(response);
				this.getRoutes();
			})
	}

	editRoute(route_id: string) {
		this.addRoute = true;
		this.settingService.getUserGroupRight(route_id)
			.subscribe(route => {
				console.log(route);
				this.newRoute = route;
			})
	}

	logNewRouteCheckbox(method: string, checked: boolean) {
		if (!this.newRoute.methods) {
			this.newRoute.methods = [''];
		}
		if (checked) {
			var index = this.newRoute.methods.indexOf(method);
			if (index == -1) { // check if the method does exist already or not
				this.newRoute.methods.push(method);
			}
		} else {
			var index = this.newRoute.methods.indexOf(method);
			this.newRoute.methods.splice(index, 1);
		}
	}

	doesRouteHasMethod(routeId: string, method: string) {
		var thisRoute = this.getRoute(routeId);
		var index = thisRoute.methods.indexOf(method);
		if (index != -1) { // check if the group does exist already or not
			return true;
		}
		return false;
	}

	doesRouteHasGroupMethod(routeId: string, groupId: string, method: string) {
		// var thisRoute = this.getRoute(routeId);
		var index = this.getGroupIndexForMethod(routeId, groupId, method);
		if (index != -1) { // check if the group does exist already or not
			return true;
		}
		return false;
	}

	getGroupIndexForMethod(routeId: string, groupId: string, method: string) {
		var thisRoute = this.getRoute(routeId);
		if (method == 'GET') {
			var index = thisRoute.getGroups.indexOf(groupId);
		}
		if (method == 'PUT') {
			var index = thisRoute.putGroups.indexOf(groupId);
		}
		if (method == 'POST') {
			var index = thisRoute.postGroups.indexOf(groupId);
		}
		if (method == 'DELETE') {
			var index = thisRoute.deleteGroups.indexOf(groupId);
		}
		return index;
	}

	getRoute(routeId: string) {
		return this.routes.find(route => route._id === routeId);
	}


	logCheckbox(userGroupId: string, routeId: string, method: string, checked: boolean) {
		// console.log(userGroupId, routeId, method, checked);
		var thisRoute = this.getRoute(routeId);
		var index = this.getGroupIndexForMethod(routeId, userGroupId, method);
		if (checked) {
			if (index == -1) { // check if the group does exist already or not
				if (method == 'GET') {
					var index = thisRoute.getGroups.push(userGroupId);
				}
				if (method == 'PUT') {
					var index = thisRoute.putGroups.push(userGroupId);
				}
				if (method == 'POST') {
					var index = thisRoute.postGroups.push(userGroupId);
				}
				if (method == 'DELETE') {
					var index = thisRoute.deleteGroups.push(userGroupId);
				}
			}
		} else {
			if (method == 'GET') {
				var index = thisRoute.getGroups.splice(index, 1);
			}
			if (method == 'PUT') {
				var index = thisRoute.putGroups.splice(index, 1);
			}
			if (method == 'POST') {
				var index = thisRoute.postGroups.splice(index, 1);
			}
			if (method == 'DELETE') {
				var index = thisRoute.deleteGroups.splice(index, 1);
			}
		}
		this.settingService.updateUserGroupRight(thisRoute)
			.subscribe(response => {
				console.log(response);
				this.getRoutes();
			})
	}
}