import { Component, OnInit } from '@angular/core';

import { IssuesService } from '../../services/issue.service';

@Component({
    moduleId: module.id,
    selector: 'app-issues',
    templateUrl: 'issues.component.html',
    providers: [IssuesService]
})
export class IssuesComponent implements OnInit {
    issues: any[];

    constructor(
        private issueService: IssuesService
    ) { }

    ngOnInit() {
        this.getIssues();
    }

    getIssues() {
        this.issueService.getIssues()
            .subscribe(issues => {
                if (issues.success !== false) {
                    this.issues = issues;
                    console.log(issues);
                }
            })
    }
}