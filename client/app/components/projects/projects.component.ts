import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ProjectService } from '../../services/project.service';

@Component({
    moduleId: module.id,
    selector: 'app-projects',
    templateUrl: 'projects.component.html',
    providers: [ProjectService]
})
export class ProjectsComponent implements OnInit {

    projects: any[];

    constructor(
        private projectService: ProjectService,
        private router: Router
    ) { }

    ngOnInit() {
        this.getProjects();
     }

    getProjects(){
        this.projectService.getProjects()
            .subscribe(projects => {
                this.projects = projects;
                console.log(projects);
            })
    }

    addProject(){
        console.log('let\'s create a project')
		this.router.navigate(['/project', 'new']);
    }
}