import { Component, OnInit, Inject, } from '@angular/core';
import { ApiConfig } from '../../config/api.config';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { UsersService } from '../../services/user.service';

@Component({
    moduleId: module.id,
    selector: 'app-login',
    templateUrl: 'login.component.html',
    providers: [ApiConfig]
})
export class LoginComponent implements OnInit {

    newUser: any = {};
    dbUser: any = {};
    alert: any = {};

    constructor(
        private userService: UsersService,
        private router: Router,
    ) {  }

    ngOnInit() { }

    createAlert(type: string, description: string) {
        this.alert.type = type;
        this.alert.description = description;
    }

    login() {
        if (this.newUser.email && this.newUser.password) {
            // Go get the user and if he can log in then set sessionStorage
            this.userService.getUserByEmail(this.newUser.email)
                .subscribe(user => {
                    this.dbUser = user;
                    if (this.dbUser) {
                        if (this.dbUser.canLogIn === true) {
                            this.userService.getUserToken(this.newUser.email, this.newUser.password)
                                .subscribe(token => {
                                    console.log(token)
                                    if (token.success) {
                                        sessionStorage.setItem('token', token.token);
                                        this.userService.createLoginEvent('login triggered');
                                        // sessionStorage.setItem('user', JSON.stringify(this.dbUser));
                                        this.router.navigate(['/home']);
                                    } else {
                                        this.createAlert('danger', 'Error while getting the token: ' + token.message);
                                    }
                                })
                        } else {
                            this.createAlert('danger', 'this user cannot log in');
                        }
                    } else {
                        this.createAlert('info', 'This user doesn\'t exist');
                    }
                })
        }
    }
}