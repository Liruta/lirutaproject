import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';

import { UsersService } from '../../services/user.service';
import { SettingsService } from '../../services/settings.service';

@Component({
	moduleId: module.id,
	selector: 'app-user-details',
	templateUrl: 'user-details.component.html',
})
export class UserDetailsComponent implements OnInit {
	user: any;
	edit = false;
	userGroups: any[];


	constructor(
		private usersService: UsersService,
		private settingsService: SettingsService,
		private route: ActivatedRoute,
		private router: Router,
		private location: Location
	) { }

	ngOnInit() {
		this.getUser();
		this.getUserGroups();
	}

	getUser() {
		this.route.params.forEach((params: Params) => {
			let id = params['user_id'];
			this.usersService.getUser(id)
				.subscribe(user => {
					this.user = user;
				});
		});
	}

	getUserGroups() {
		this.settingsService.getUserGroups()
			.subscribe(userGroups => {
				this.userGroups = userGroups;
			})
	}

	goBack() {
		this.location.back();
	}

	toggleEditUser() {
		this.edit = !this.edit;
	}

	logCheckbox(groupId: any, checked: any) {
		console.log(groupId, checked);
		if (checked) {
			var index = this.user.groups.indexOf(groupId);
			if (index == -1) { // check if the group does exist already or not
				this.user.groups.push(groupId);
			}
		} else {
			var index = this.user.groups.indexOf(groupId);
			this.user.groups.splice(index, 1);
		}
		console.log(this.user);
	}

	saveUser() {
		this.usersService.updateUser(this.user)
			.subscribe(response => {
				console.log(response);
				this.goBack();
			})
	}

	getUserGroup(groupId: string) {
		return this.userGroups.find(group => group._id === groupId);
	}

	doesUserHaveGroup(groupId: string) {
		var index = this.user.groups.indexOf(groupId);
		if (index != -1) { // check if the group does exist already or not
			return true;
		}
		return false;
	}
}