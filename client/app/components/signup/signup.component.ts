import { Component, OnInit } from '@angular/core';
import { ApiConfig } from '../../config/api.config';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { UsersService } from '../../services/user.service';

@Component({
    moduleId: module.id,
    selector: 'app-signup',
    templateUrl: 'signup.component.html',
    providers: [UsersService]
})
export class SignupComponent implements OnInit {
    newUser: any = {};
    data: any;
    alert: any = {};
    constructor(
        private userService: UsersService,
        private router: Router,
    ) { }

    ngOnInit() { }

    checkNewUser() {
        if (this.newUser.password != this.newUser.cpassword) {
            this.newAlert('danger', 'The password doesn\'t match');
            // alert('the 2 password doesn\'t match');
            return false;
        } else {
            this.newAlert('info', 'your account will be created');
            return true;
        }
    }

    signup() {
        if (this.checkNewUser()) {
            // Go in user sertvice and post a new user
            this.userService.postUsers(this.newUser)
                .subscribe(data => {
                    this.data = data;
                    console.log(data);
                    if (data.status == 'Success') {
                        this.newAlert('success', 'your account has been created');
                    } else {
                        this.newAlert('danger', 'Something went wrong while creating your account');
                    }

                })
        }
    }

    newAlert(type: string, message: string) {
        this.alert = {
            type: type,
            message: message
        }
    }
}