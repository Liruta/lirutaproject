import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UsersService } from '../../services/user.service';
import { SettingsService } from '../../services/settings.service';


@Component({
	moduleId: module.id,
	selector: 'app-users',
	templateUrl: 'users.component.html',
})
export class UsersComponent implements OnInit {
	users: any[];
	userGroups: any[];
	constructor(
		private usersService: UsersService,
		private settingsService: SettingsService,
		private router: Router
	) { }

	ngOnInit() {
		this.getUsers();
		this.getUserGroups();
	}

	getUsers() {
		this.usersService.getUsers()
			.subscribe(users => {
				this.users = users;
				console.log(this.users)
			})
	}

	getUserGroups() {
		this.settingsService.getUserGroups()
			.subscribe(userGroups => {
				this.userGroups = userGroups;
			})
	}

	goToDetails(user_id: string) {
		this.router.navigate(['/user', user_id]);
	}

	getUserGroup(groupId: string){
		return this.userGroups.find(group => group._id === groupId);
	}
}