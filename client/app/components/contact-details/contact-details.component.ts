import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';

import { ContactsService } from '../../services/contact.service';

@Component({
	moduleId: module.id,
	selector: 'app-contact-details',
	templateUrl: 'contact-details.component.html',
	providers: [ContactsService]
})
export class ContactDetailsComponent implements OnInit {
	contact: any;
	response: any;
	edit = false;
	constructor(
		private contactsService: ContactsService,
		private route: ActivatedRoute,
		private router: Router,
		private location: Location
	) { }

	ngOnInit() {
		this.getContact();
	}

	getContact() {
		this.route.params.forEach((params: Params) => {
			let id = params['contact_id'];
			if (id == 'new') {
				this.edit = true;
				return this.contact = {};
			}
			this.contactsService.getContact(id)
				.subscribe(contact => {
					this.contact = contact;
				});
		});
	}

	toggleEditContact() {
		this.edit = !this.edit;
	}

	saveContact() {
		console.log(this.contact);
		if (!this.contact._id) {
			this.contactsService.postContact(this.contact)
				.subscribe(response => {
					this.response = response;
					this.router.navigate(['/contacts']);
				})
		} else {
			this.contactsService.updateContact(this.contact)
				.subscribe(response => {
					this.response = response;
					this.toggleEditContact();
				})
		}
	}

	deleteContact(contact_id: any){
		if(contact_id){
			this.contactsService.deleteContact(contact_id)
				.subscribe(response => {
					this.response = response;
					this.router.navigate(['/contacts']);
				})
		} else {
			return false;
		}
	}

	goBack() {
		this.location.back();
	}

}