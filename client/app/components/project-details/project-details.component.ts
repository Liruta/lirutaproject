import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';

import { ProjectService } from '../../services/project.service';

@Component({
	moduleId: module.id,
	selector: 'app-project-details',
	templateUrl: 'project-details.component.html',
	providers: [ProjectService]
})
export class ProjectDetailsComponent implements OnInit {
	edit: boolean;
	project: any;
	id: string;
	constructor(
		private projectService: ProjectService,
		private route: ActivatedRoute,
		private router: Router,
		private location: Location
	) { }

	ngOnInit() {
		this.getProject();
	}

	getProject() {
		this.route.params.forEach((params: Params) => {
			this.id = params['project_id'];
			if( this.id == 'new'){
				this.edit = true;
				this.project = {userInvolved: []};
				return false;
			}
			this.projectService.getProject(this.id)
				.subscribe(project => {
					this.project = project;
					console.log(project);
				})
		})
	}

	toggleEdit() {
		this.edit = !this.edit;
	}

	saveProject() {
		if (this.project._id) {
			// update
			this.projectService.updateProject(this.project)
				.subscribe(response => {
					console.log(response);
					this.toggleEdit();
				})
		} else {
			// create project
			this.projectService.postProject(this.project)
				.subscribe(response => {
					console.log(response);
					this.location.back();
				})
		}
	}

	deleteProject(){
		this.projectService.deleteProject(this.project._id)
			.subscribe(response => {
				console.log(response);
				this.router.navigate(['/projects']);
			})
	}
}