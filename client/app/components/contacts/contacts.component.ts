import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ContactsService } from '../../services/contact.service';

@Component({
	moduleId: module.id,
	selector: 'app-contacts',
	templateUrl: 'contacts.component.html',
	providers: [ContactsService]

})
export class ContactsComponent implements OnInit {
	constructor(
		private contactsService: ContactsService,
		private router: Router
	) { }

	contacts: any[];

	ngOnInit() {
		this.getContacts()
	}

	getContacts() {
		this.contactsService.getContacts()
			.subscribe(contacts => {
				if (contacts.success !== false) {
					this.contacts = contacts;
				} else {
					console.log(contacts.message);
				}
				// console.log(this.contacts);
			});
	}

	goToDetails(id: string) {
		this.router.navigate(['/contact', id]);
		// console.log('Go to contact ' + id)
	}

	addContact() {
		let id = 'new';
		this.router.navigate(['/contact', id]);
	}
}