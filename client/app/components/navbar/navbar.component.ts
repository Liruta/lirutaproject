import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { UsersService } from '../../services/user.service';

@Component({
	moduleId: module.id,
	selector: 'app-navbar',
	templateUrl: 'navbar.component.html',
})
export class NavbarComponent implements OnInit {
	token: any = {};
	tokenDecoded: any = {};

	constructor(
		private usersService: UsersService,
		private router: Router,
	) {
		usersService.loginEventObs.subscribe(
			loginString => {
				this.getUser();
			}
		)
	}

	ngOnInit() {
		this.getUser();
	}

	getUser() {
		this.token = sessionStorage.getItem('token');
		if (this.token) {
			this.usersService.readUserToken(this.token)
				.subscribe(tokenDecoded => {
					this.tokenDecoded = tokenDecoded;
					console.log(tokenDecoded)
				})
		} else {
			this.tokenDecoded = false;
		}
	}

	logout() {
		sessionStorage.removeItem('token');
		this.getUser();
		this.router.navigate(['/login']);
	}
}