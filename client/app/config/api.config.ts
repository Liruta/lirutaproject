import { Injectable } from '@angular/core';

@Injectable()
export class ApiConfig {

    constructor() { }

	// Main route
    private url = 'http://localhost:4200/api/';

	// App Routes
    private contactsUrl = this.url + 'contacts';
	private contactUrl = this.url + 'contact/';
	private usersUrl = this.url + 'users';
	private userUrl = this.url + 'user/';
	private authUrl = this.url + 'authenticate';  
	private issuesUrl = this.url + 'issues';  
	private issueUrl = this.url + 'issue/';  
	private projectsUrl = this.url + 'projects';  
	private projectUrl = this.url + 'project/';  

	// Setting Routes
	private userGroupsUrl = this.url + 'settings/usergroups';
	private userGroupUrl = this.url + 'settings/usergroup/';
	private userGroupsRightsUrl = this.url + 'settings/usergrouprights';
	private userGroupsRightUrl = this.url + 'settings/usergroupright/';
}