import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'my-app',
    templateUrl: 'app.component.html'
})
export class AppComponent { 
    isLoggedIn = localStorage.getItem('isLoggedIn');

    logout(){
        localStorage.removeItem('isLoggedIn');
    }
}
